set -e
echo "Getting Kubernetes cluster info..."
kubectl cluster-info
# Determinar si el Tiller ya fue instalado
export K8S_TILLER=true
kubectl -n kube-system get serviceaccount | grep tiller || export K8S_TILLER=false
if [[ "$K8S_TILLER" = false ]]
then
    echo "Creating service account and cluster role binding for Tiller..."
    kubectl create serviceaccount -n kube-system tiller || true
    kubectl create clusterrolebinding tiller --clusterrole=cluster-admin --serviceaccount=kube-system:tiller || true
    echo "Initializing Helm..."
    helm init --service-account tiller --history-max 10 || true
    echo "Creating service account for Dashboard"
    kubectl create clusterrolebinding kubernetes-dashboard --clusterrole=cluster-admin --serviceaccount=kube-system:kubernetes-dashboard || true
    echo "Helm has been installed."
else
        echo "Helm already installed."
fi